import { CubeObject, FaceObject, RubikCipherConfig, ScrambleObject, RubikKeyParts } from './RubikCipherInterfaces';
const Cube = require('bedard-cube');

import {
  chunkString, minimalSizeByText, getInverseCombination,
  canFitInCube, getAllIndexes, encodeScramble, decodeScramble,
} from './helpers/rubikHelpers';

enum FillAlpabet {
  default = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
  czech = 'aábcčdďeéěfghiíjklmnňoópqrřsštťuúůvwxyýzžAÁBCČDĎEÉĚFGHIÍJKLMNŇOÓPQRŘSŠTŤUÚŮVWXYÝZŽ0123456789',
}

class RubikCipher {
  private text: string = '';
  private cube: any = undefined;
  private currentScramble: ScrambleObject[] = [];

  private size: number = 2;
  private groupSize: number = 1;
  private fillRest: boolean = true;
  private fillWithRandom: boolean = false;
  private fillAlphabet: FillAlpabet = FillAlpabet.default;

  /**
   * Key constructor (Usually used when decoding)
   * @param {string} text Ciphre text
   * @param {string} config Cipthre key
   */
  constructor(text: string, config?: string);
  /**
   * Config constructor (Usually used when encoding)
   * @param {string} text Ciphre text 
   * @param {RubikCipherConfig} config Ciphre config object 
   */
  constructor(text: string, config?: RubikCipherConfig);
  constructor(text: string, config?: RubikCipherConfig | string) {
    if (text === '') {
      throw new Error('Text cannot be empty');
    }

    this.text = text;

    if (config) {
      if (typeof config === 'string') {
        this.setupFromKey(config);
      } else {
        if (typeof config.cubeSize !== 'undefined') {
          if (config.cubeSize < 2) {
            throw new Error('Cube size must be larger than 2');
          }

          if (!canFitInCube(this.text, config.cubeSize)) {
            throw new Error(
              `Text of length ${this.text.length} cant fit in cube with size ${config.cubeSize}`,
            );
          }

          this.size = config.cubeSize;
        } else {
          this.size = minimalSizeByText(text);
        }

        if (typeof config.groupSize !== 'undefined') {
          if (config.groupSize < 1) {
            throw new Error('Group size must be larger than 0');
          }

          this.groupSize = config.groupSize;
        } else {
          this.groupSize = 1;
        }

        if (typeof config.fillRest !== 'undefined') {
          this.fillRest = config.fillRest;
        } else {
          this.fillRest = true;
        }

        if (typeof config.fillWithRandom !== 'undefined') {
          this.fillWithRandom = config.fillWithRandom;
        } else {
          this.fillWithRandom = false;
        }

        this.reset();
      }
    } else {
      this.size = minimalSizeByText(text);
      this.reset();
    }
  }

  private setupFromKey(key: string): void {
    const { combinationText, config } = this.getKeyParts(key);
    this.size = config.cubeSize;
    this.groupSize = config.groupSize;
    this.fillRest = config.fillRest;
    this.fillWithRandom = config.fillWithRandom;
    this.reset();

    this.applyReverseCombination(this.combinationTextToObject(combinationText));
  }

  private isValidKey(key: string): boolean {
    if (!(/^[^+](\+{0,1}[- a-zA-Z0-9]+)+[^+]$/.test(key))) {
      return false;
    }

    const keyParts = key.split('+');

    if (keyParts.length !== 5) {
      return false;
    }

    const cubeSize = Number(keyParts[0]);
    const groupSize = Number(keyParts[1]);

    if (cubeSize < 2) {
      return false;
    }

    if (groupSize < 1) {
      return false;
    }

    if (!canFitInCube(this.text, cubeSize)) {
      return false;
    }

    return true;
  }

  private getKeyParts(key: string): RubikKeyParts {
    if (this.isValidKey(key)) {
      const keyParts = key.split('+');
      return {
        config: {
          cubeSize: Number(keyParts[0]),
          groupSize: Number(keyParts[1]),
          fillRest: (keyParts[2] === '1'),
          fillWithRandom: (keyParts[3] === '1'),
        },
        combinationText: keyParts[4],
      };
    }
    throw new Error('Key is invalid');
  }

  private combinationTextToObject(combination: string): ScrambleObject[] {
    return combination.split(' ').map(decodeScramble);
  }

  private applyReverseCombination(combination: ScrambleObject[]): void {
    this.turn(getInverseCombination(combination));
  }

  /**
   * Function for cube scrambling
   * @param {number} count How many times you want to scramble 
   */
  public scramble(count: number = 0): void {
    this.reset();
    const newScramble: Array<ScrambleObject> = this.cube.generateScramble(count);
    this.currentScramble = newScramble;
    this.cube.turn(this.currentScramble);
  }

  /**
   * Function for additional cube scrambling
   * @param {number} count How many times you want to scramble 
   */
  public addScramble(count: number = 0): void {
    const newScramble: Array<ScrambleObject> = this.cube.generateScramble(count);
    this.currentScramble = this.currentScramble.concat(newScramble);
    this.cube.turn(this.currentScramble);
  }

  /**
   * Function to manually turn the cube
   * @param {string | Array<ScrambleObject>} turnKey selected sequence of turns
   */
  public turn(turnKey: string | Array<ScrambleObject>): void {
    const newScramble: Array<ScrambleObject> = [];
    if (typeof turnKey === 'string') {
      const turnKeys = turnKey.split(' ').map(decodeScramble);
      newScramble.push(...turnKeys);
    } else {
      newScramble.push(...turnKey);
    }

    this.currentScramble = this.currentScramble.concat(newScramble);
    this.cube.turn(newScramble);
  }

  /**
   * Reset the cube or create new one when cube is undefined
   */
  public reset(): void {
    if (typeof this.cube !== 'undefined') {
      this.cube.reset();
    } else {
      this.cube = new Cube(this.size, { useObjects: true });
    }
  }

  /**
   * Getter for current cube Combination string
   * @returns {string}
   */
  public get Combination(): string {
    return this.currentScramble.map(encodeScramble).join(' ');
  }

  public get InverseCombination(): ScrambleObject[] {
    if (this.currentScramble.length > 0) {
      return getInverseCombination(this.currentScramble);
    }
    return [];
  }

  /**
   * Getter for current text from cube stickers
   * @returns {string}
   */
  public get Text(): string {
    const cubeIndexes = getAllIndexes(this.cube, this.size);
    const textSplit = this.text.split('');
    const alphabetSplit = this.fillAlphabet.split('');
    let returnString = '';

    if (this.groupSize > 1) {
      const groups = chunkString(this.text, this.groupSize) as string[];
      if (groups[groups.length - 1].length < this.groupSize) {
        while (groups[groups.length - 1].length < this.groupSize) {
          if (this.fillRest) {
            if (groups.join('').length < this.CubeTileCount) {
              if (this.fillWithRandom) {
                groups[groups.length - 1] += alphabetSplit[
                  Math.floor(Math.random() * alphabetSplit.length)
                ];
              } else {
                groups[groups.length - 1] += 'x';
              }
            }
          } else {
            break;
          }
        }
      }

      while (groups.length < this.CubeTileCount) {
        let textGroup = '';
        for (let i = 0; i < this.groupSize; i++) {
          if (this.fillRest) {
            if (this.fillWithRandom) {
              textGroup += alphabetSplit[
                Math.floor(Math.random() * alphabetSplit.length)
              ];
            } else {
              textGroup += 'x';
            }
          } else {
            continue;
          }
        }
        groups.push(textGroup);
      }

      const mappedString = cubeIndexes.map(i => groups[i]);
      returnString = mappedString.join('');
    } else {
      const mappedString = cubeIndexes.map((i) => {
        if (textSplit[i] !== undefined) {
          return textSplit[i];
        }
        if (this.fillRest) {
          if (this.fillWithRandom) {
            return alphabetSplit[Math.floor(Math.random() * alphabetSplit.length)];
          }
          return 'x';
        }
        return '';
      });
      returnString = mappedString.join('');
    }

    return returnString;
  }

  public get Indexes(): number[] {
    return getAllIndexes(this.cube, this.size);
  }

  /**
   * Gets current cube key in format size+groupSize+fillRest(0 or 1)+fillRandom(0 or 1)+combination
   * @returns {string} key
   */
  public get Key(): string {
    return [
      this.size,
      this.groupSize,
      (this.fillRest ? 1 : 0),
      (this.fillWithRandom ? 1 : 0),
      this.Combination,
    ].join('+');
  }

  public get CubeTileCount(): number {
    return 6 * (this.size ** 2);
  }
}

export = RubikCipher;