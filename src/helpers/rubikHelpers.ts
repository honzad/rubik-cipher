import { ScrambleObject, FaceObject, RubikCipherConfigRequired } from '../RubikCipherInterfaces';
const Cube = require('bedard-cube');

export const chunkString = (str: string, length: number) => str.match(new RegExp('.{1,' + length + '}', 'g'));

export const minimalSizeByText = (text: string): number => {
  let sizeNumber = 2;
  while (text.length > (6 * (sizeNumber * sizeNumber))) {
    sizeNumber++;
  }
  return sizeNumber;
};

export const canFitInCube = (text: string, size: number): boolean => (
  text.length <= (6 * (size ** 2))
);

const createInverseTurn = (turnObj: ScrambleObject): ScrambleObject[] => {
  const { rotation } = turnObj;
  const retArr: ScrambleObject[] = [];

  if (rotation === -1) {
    retArr.push({
      ...turnObj,
      rotation: 1,
    });
  } else if (rotation === 1) {
    retArr.push({
      ...turnObj,
      rotation: -1,
    });
  } else { // Rotation === 2
    retArr.push({
      ...turnObj,
      rotation: -1,
    }, {
      ...turnObj,
      rotation: -1,
    });
  }

  return retArr;
};

export const getInverseCombination = (combination: ScrambleObject[]): ScrambleObject[] => (
  combination.map(c => createInverseTurn(c)).flat().reverse()
);

const getIndex = (obj: FaceObject, size: number): FaceObject => ({
  ...obj,
  index: (obj.value * (size ** 2)) + obj.originalIndex,
});

const getStickers = (cube: any): FaceObject[] => {
  const stickers: FaceObject[] = [];
  cube.stickers((sticker: any) => {
    stickers.push({
      ...sticker,
      index: 0,
    });
  });
  return stickers;
};

const getIndexMap = (cube: any, size: number): FaceObject[] => (
  getStickers(cube).map((sticker: FaceObject) => getIndex(sticker, size))
);

const getIndexOnly = (indexMap: FaceObject[]): number[] => indexMap.map(index => index.index);

export const getAllIndexes = (cube: any, size: number) => getIndexOnly(getIndexMap(cube, size));

export const isValidKey = (text: string, key: string): boolean => {
  if (!(/^[^+](\+{0,1}[- a-zA-Z0-9]+)+[^+]$/.test(key))) {
    return false;
  }

  const keyParts = key.split('+');

  if (keyParts.length !== 5) {
    return false;
  }

  const cubeSize = Number(keyParts[0]);
  const groupSize = Number(keyParts[1]);

  if (cubeSize < 2) {
    return false;
  }

  if (groupSize < 1) {
    return false;
  }

  if (!canFitInCube(text, cubeSize)) {
    return false;
  }

  return true;
};

export const getDataFromKey = (text: string, key: string): RubikCipherConfigRequired => {
  if (isValidKey(text, key)) {
    const keyParts = key.split('+');
    return {
      cubeSize: Number(keyParts[0]),
      groupSize: Number(keyParts[1]),
      fillRest: (keyParts[2] === '1'),
      fillWithRandom: (keyParts[3] === '1'),
    };
  }

  throw new Error('Key is not valid');
};

export const encodeScramble = (turnObj: ScrambleObject): string => {
  const { depth, target, wide, rotation } = turnObj;
  const isWide = wide ? '1' : '0';

  let rot = '';
  switch (rotation) {
    case 1:
    case 2:
      rot = rotation.toString();
      break;
    case -1:
      rot = '3';
      break;
    default:
      throw new Error('Invalid spin count');
  }

  return `${target}${isWide}${depth.toString()}${rot}`;
};

export const decodeScramble = (turnStr: string): ScrambleObject => {
  const splitString = turnStr.split('');
  if (splitString.length >= 4) {
    const target = splitString.shift() as string;
    const wide = splitString.shift() === '1';
    let rotation = Number(splitString.pop());
    if (rotation === 3) {
      rotation = -1;
    }
    const depth = Number(splitString);

    return { target, wide, rotation, depth };
  }

  throw new Error('Invalid scramble string format');
};